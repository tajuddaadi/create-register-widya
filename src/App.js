import React from 'react'
// import logo from './logo.svg';
import './App.css';
import Register from "./components/register/Register";

class App extends React.Component {
  render(){
    return (
      <div className="app flex-row align-items-center background-login" style={{height: "auto", padding: "10px"}}>
        <div className="container">
          <div className="justify-content-center row">
            <Register/>
          </div>
        </div>
      </div>
  
    );
  }
  
}

export default App;
