import './Form.css';
import widya from "../form/widya.png"

const Form = () => {
    return (
        <form action="#"> 
            <div className="logo text-center">
                <img src={widya} className="logo-widya-skilloka" alt="logo-widya-skilloka"></img>
                <h5><b>Data Perusahaan</b></h5>
                <div className="row">
                    <div className="col-sm-6">
                        <div className="w-100 form-group">
                            <label hmtlFor="companyName" className="input-label">Nama Perusahaan</label>
                            <input label="Nama Perusahaan" id="companyName" name="companyName" type="text" className="form-control"></input>
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className="w-100 form-group">
                            <label htmlFor="companyDomain" className="input-label">Website Perusahaan</label>
                            <input label="Website Perusahaan" id="companyDomain" name="companyDomain" type="text" className="form-control"></input>
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <label htmlFor="badan-usaha" className="input-label">Nomor Telepon Perusahaan</label>
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text">+</span>
                            </div>
                            <input pattern="[0-9]*" inputMode="numeric" type="text" className="form-control" name="companyPhone" id="companyPhone" placeholder="62"></input>
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <label htmlFor="badan-usaha" className="input-label">Bidang Perusahaan</label>
                        <div className=" css-2b097c-container">
                            <div className=" css-yk16xz-control">
                                <div className=" css-1hwfws3">
                                    <div className=" css-1wa3eu0-placeholder">Select...
                                    </div>
                                    <div className="css-1g6gooi">
                                        <div className="" style={{display: "inline-block"}}>
                                            <input autoCapitalize="none" autoComplete="off" autoCorrect="off" id="react-select-2-input" spellCheck="false" tabIndex="0" type="text" aria-autocomplete="list" ></input>
                                            <div style={{position: "absolute", top: "0px", left: "0px", visibility: "hidden", height: "0px", overflow: "scroll", }}>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className=" css-1wy0on6">
                                    <span className=" css-1okebmr-indicatorSeparator"></span>
                                    <div aria-hidden="true" className=" css-tlfecz-indicatorContainer">
                                        <svg height="20" width="20" viewBox="0 0 20 20" aria-hidden="true" focusable="false" className="css-19bqh2r">
                                            <path d="M4.516 7.548c0.436-0.446 1.043-0.481 1.576 0l3.908 3.747 3.908-3.747c0.533-0.481 1.141-0.446 1.574 0 0.436 0.445 0.408 1.197 0 1.615-0.406 0.418-4.695 4.502-4.695 4.502-0.217 0.223-0.502 0.335-0.787 0.335s-0.57-0.112-0.789-0.335c0 0-4.287-4.084-4.695-4.502s-0.436-1.17 0-1.615z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr></hr>
                <h5><b>Data diri Anda</b></h5>
                <div className="row">
                    <div className="col-sm-6">
                        <div className="w-100 form-group">
                            <label htmlFor="userFullName" className="input-label">Nama Lengkap</label>
                            <input label="Nama Lengkap" id="userFullName" name="userFullName" type="text" className="form-control"></input>
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className="w-100 form-group">
                        <label htmlFor="userNickName" className="input-label">Nama Panggilan</label>
                        <input label="Nama Panggilan" id="userNickName" name="userNickName" type="text" className="form-control"></input>
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className="w-100 form-group">
                        <label htmlFor="userEmail" className="input-label">Email</label>
                        <input label="Email" id="userEmail" name="userEmail" type="email" className="form-control"></input>
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <label htmlFor="user-phone" className="input-label">Telepon</label>
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text">+</span>
                            </div>
                            <input pattern="[0-9]*" inputMode="numeric" type="text" className="form-control" name="userPhone" id="userPhone" placeholder="62"></input>
                        </div>
                    </div>
                </div>
                <div className="d-flex mt-3">
                    <div className="mx-auto">
                        <div className="text-center">
                            <div>
                                <div style={{width: "304px", height: "78px"}}>
                                    <div>
                                        <iframe title="reCAPTCHA" src="https://www.google.com/recaptcha/api2/anchor?ar=2&amp;k=6LeP_rcZAAAAAAOPRPpdDhPFA4cVPvpS1uJEOeJM&amp;co=aHR0cHM6Ly9ocmlzLndpZHlhc2tpbGxva2EuY29tOjQ0Mw..&amp;hl=id&amp;type=image&amp;v=npGaewopg1UaB8CNtYfx-y1j&amp;theme=light&amp;size=normal&amp;badge=bottomright&amp;cb=fsjut9bpaxcw" width="304" height="78" role="presentation" name="a-iduupic3eob5" frameBorder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox">
                                        </iframe>
                                    </div>
                                    <textarea id="g-recaptcha-response" name="g-recaptcha-response" className="g-recaptcha-response" style={{width: "250px", height: "40px", border: "1px solid rgb(193, 193, 193)", margin: "10px 25px", padding: "0px", resize: "none", display: "none"}}></textarea>
                                </div>
                                <iframe style={{display: "none"}}></iframe>
                            </div>
                        </div>
                    </div>
                </div>   
                <button type="submit" className="login-submit mt-3 btn btn-secondary">Register</button>
                <div className="mt-5">
                    <a href="https://hris.widyaskilloka.com/login"><i className="mr-2 fa fa-chevron-left"></i> Ke Halaman Login</a>
                </div>
            </div>
        </form>
    )
}

export default Form