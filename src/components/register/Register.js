import './Register.css';
import Form from "../form/Form"

const Register = () => {
    return (
        <div className="col-sm-8 col-md-9">
            <div className="card-group" style={{height: "auto"}}>
                <div className="card-login-form card">
                    <div className="card-body" style={{padding: "0px"}}>
                        <Form />
                    </div>
                </div>

            </div>
        </div>
    )
}

export default Register